#!/bin/dash
# Create a dated text file at a specific location and append text to it.
#
# Usage:
#   $ todo.sh #to print things to do
#   $ todo.sh a new thing todo #to add a new line in the todo file
#   $ todo.sh -e #to open the todo file with an editor
#
# Produces:
#   todo in your $TODO_DIRECTORY (this is set below).

TODO_DIRECTORY="${TODO_DIRECTORY:-"${HOME}/.local/share/todo"}"
TODO_EDITOR="nvim "

TODO_FILE="todo"
TODO_PATH="${TODO_DIRECTORY}/${TODO_FILE}"

if [ ! -d "${TODO_DIRECTORY}" ]; then
    while true; do
        read -rp "${TODO_DIRECTORY} does not exist, do you want to create it? (y/n) " yn
        case "${yn}" in
            [Yy]* ) mkdir -p "${TODO_DIRECTORY}"; break;;
            [Nn]* ) exit;;
            * ) echo "Please answer y or n";;
        esac
    done
fi

if [ "$1" = "-e" ]; then

  eval ${TODO_EDITOR} ${TODO_PATH}

else

  if [ ${#} -eq 0 ]; then

    cat ${TODO_PATH}


  else

    printf "%s\n\n%s\n" "${*}" "$(<${TODO_PATH})" > "${TODO_PATH}"

  fi
fi